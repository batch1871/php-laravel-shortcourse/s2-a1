<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 3 and 4</title>
</head>
<body>
	<h1>Activity - Loop</h1>
	<p><?php echo divisibleByFive();?> </p>

	<h1>Activity - Arrays</h1>
<!-- empty array created in code.php -->

<!-- 	accept name of the student and add to array -->
	<?php array_push($students, 'John Smith'); ?>

<!-- 	//print the names added so far in the array -->
	<?php echo var_dump($students); ?>

<!-- 	count the numbers of names in the students array -->
	<p>Count: <?= count($students); ?></p>

<!-- 	Add another student -->
	<?php array_push($students, 'Jane Smith'); ?>

<!-- 	and Print again the array and it's new count -->
	<?php echo var_dump($students); ?>
	<p>Count: <?= count($students); ?></p>
<!-- finally remove the last student and print the array and its count -->
	<?php array_shift($students); ?>
	<?php echo var_dump($students); ?>
	<p>Count: <?= count($students); ?></p>

</body>
</html>